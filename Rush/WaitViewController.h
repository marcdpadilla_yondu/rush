//
//  WaitViewController.h
//  Globe Ride
//
//  Created by Marc Darren Padilla on 23/5/15.
//  Copyright (c) 2015 Section 8. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaitViewController : UIViewController {
    UIWindow *window;
    UIToolbar *bg;
}

@property (nonatomic, strong) IBOutlet UIView *progressContainer;

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;
- (id) init;
- (void) show;
- (void) hide;

@end
