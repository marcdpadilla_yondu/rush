//
//  UIManager.swift
//  Rush
//
//  Created by Marc Darren Padilla on 28/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

class UIManager: NSObject {
    class var navigationBarColor : UIColor {
        return UIColor(red: 93.0/255.0, green: 195.0/255.0, blue: 249.0/255.0, alpha: 1.0)
    }
    
    class var buttonRadius : CGFloat {
        return 3.0
    }
    
    class func configureNavigationBar(viewController : UIViewController) {
        viewController.navigationController!.navigationBar.barTintColor = navigationBarColor
//        navController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName : UIFont(name: "FSElliotPro-Bold", size: 17.0)!]
        viewController.navigationController!.navigationBar.translucent = false
        viewController.navigationController!.navigationBar.barStyle = .BlackTranslucent
        
        let revealViewController : SWRevealViewController? = viewController.revealViewController()
        if revealViewController != nil {
            
            let b = UIButton(type: .Custom)
            b.addTarget(viewController.revealViewController(), action: Selector("revealToggle:"), forControlEvents: .TouchUpInside)
            b.setImage(UIImage(named: "nav"), forState: .Normal)
            b.frame = CGRectMake(0, 0, (b.imageView?.image!.size.width)!, (b.imageView?.image!.size.height)!)
            
            let buttonItem = UIBarButtonItem(customView: b)
            
            buttonItem.target = viewController.revealViewController()
            buttonItem.action = Selector("revealToggle:")
            viewController.navigationItem.leftBarButtonItem = buttonItem
            viewController.navigationController?.navigationBar.addGestureRecognizer(viewController.revealViewController().panGestureRecognizer())
        }
    }
    
    class func configureNavigationBar(navController : UINavigationController, bgColor : UIColor, titleColor : UIColor, barStyle : UIBarStyle) {
        navController.navigationBar.barTintColor = bgColor
        navController.navigationBar.backgroundColor = bgColor
//        navController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : titleColor, NSFontAttributeName : UIFont(name: "FSElliotPro-Bold", size: 17.0)!]
        navController.navigationBar.translucent = false
        navController.navigationBar.barStyle = barStyle
    }
}
