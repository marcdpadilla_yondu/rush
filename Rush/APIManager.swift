//
//  APIManager.swift
//  Rush
//
//  Created by Marc Darren Padilla on 28/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

class APIManager: NSObject {
    
    class var ServerIP : String {
        return "http://54.179.169.231/"
    }
    
    class var APIBaseURL : String {
        return "\(ServerIP)kollab/index.php/api/"
    }
    
    class var MerchantID : String {
        return "15"
    }
    
    class func paramsToString (params : NSDictionary) -> NSString {
        var ret : String = ""
        
        for k : String in params.allKeys as! [String] {
            ret += "\(k)=\(params[k]!)&"
        }
        
        let cleanString = String(ret.characters.dropLast())
        
        print(cleanString)
        
        return NSString(string : cleanString).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLUserAllowedCharacterSet())!
    }
    
    class func registerUser(urlSession : NSURLSession, params : NSDictionary, completion : (data : NSData, error : NSError?) -> Void, failure : (data : NSData?, error : NSError?) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: "\(APIBaseURL)kollabcontroller/register.json")!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 30.0)
        request.HTTPMethod = "POST"
        request.HTTPBody = paramsToString(params).dataUsingEncoding(NSUTF8StringEncoding)
        
//        print(request.URL?.absoluteString)
        
        let dataTask = urlSession.dataTaskWithRequest(request) { (data, response, error) -> Void in
            if data == nil {
                failure(data: data, error: error)
            } else {
                completion(data: data!, error: error)
            }
            
        }
        
        dataTask.resume()
        
    }
    
    class func login(urlSession : NSURLSession, params : NSDictionary, completion : (data : NSData, error : NSError?) -> Void, failure : (data : NSData?, error : NSError?) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: "\(APIBaseURL)authcontroller/customerLogin.json")!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 30.0)
        request.HTTPMethod = "POST"
        request.HTTPBody = paramsToString(params).dataUsingEncoding(NSUTF8StringEncoding)
        
        print(paramsToString(params))
        
        let dataTask = urlSession.dataTaskWithRequest(request) { (data, response, error) -> Void in
            if data == nil {
                failure(data: data, error: error)
            } else {
                completion(data: data!, error: error)
            }
            
        }
        
        dataTask.resume()
        
    }
    
    class func getRedeemableProducts(urlSession : NSURLSession, params : NSDictionary, completion : (data : NSData, error : NSError?) -> Void, failure : (data : NSData?, error : NSError?) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: "\(APIBaseURL)kollabcontroller/redeemableProducts.json?\(paramsToString(params))")!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 30.0)
        request.HTTPMethod = "GET"
        
        
        let dataTask = urlSession.dataTaskWithRequest(request) { (data, response, error) -> Void in
            if data == nil {
                failure(data: data, error: error)
            } else {
                completion(data: data!, error: error)
            }
            
        }
        
        dataTask.resume()
        
    }
    
    class func getAppDetails(urlSession : NSURLSession, params : NSDictionary, completion : (data : NSData, error : NSError?) -> Void, failure : (data : NSData?, error : NSError?) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: "\(APIBaseURL)kollabcontroller/getAppDetails.json?\(paramsToString(params))")!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 30.0)
        request.HTTPMethod = "GET"
        
        
        let dataTask = urlSession.dataTaskWithRequest(request) { (data, response, error) -> Void in
            if data == nil {
                failure(data: data, error: error)
            } else {
                completion(data: data!, error: error)
            }
            
        }
        
        dataTask.resume()
        
    }
    
    
    class func validateQRCode(urlSession : NSURLSession, params : NSDictionary, completion : (data : NSData, error : NSError?) -> Void, failure : (data : NSData?, error : NSError?) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: "\(APIBaseURL)kollabcontroller/validateQRCode.json")!, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 30.0)
        request.HTTPMethod = "POST"
        request.HTTPBody = paramsToString(params).dataUsingEncoding(NSUTF8StringEncoding)
        
        print(paramsToString(params))
        
        let dataTask = urlSession.dataTaskWithRequest(request) { (data, response, error) -> Void in
            if data == nil {
                failure(data: data, error: error)
            } else {
                completion(data: data!, error: error)
            }
            
        }
        
        dataTask.resume()
        
    }
}
