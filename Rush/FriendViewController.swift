//
//  FriendViewController.swift
//  Rush
//
//  Created by Marc Darren Padilla on 29/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

class FriendViewController: UIViewController {

    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var pointsLabel: UILabel!
    
    func customSetup() {
        let pointsString = NSString(string: pointsLabel.text!)
        let attributedString = NSMutableAttributedString(string: pointsString as String, attributes: [NSFontAttributeName : pointsLabel.font, NSForegroundColorAttributeName : pointsLabel.textColor])
        attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(20), range: pointsString.rangeOfString("PTS"))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.blackColor(), range: pointsString.rangeOfString("PTS"))
        attributedString.addAttribute(NSBaselineOffsetAttributeName, value: 17.0, range: pointsString.rangeOfString("PTS"))
        pointsLabel.attributedText = attributedString
        
        profileImg.layer.cornerRadius = 88.0
        profileImg.layer.borderWidth = 2.0
        profileImg.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        customSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
