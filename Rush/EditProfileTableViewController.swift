//
//  EditProfileTableViewController.swift
//  Rush
//
//  Created by Marc Darren Padilla on 28/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

class EditProfileTableViewController: UITableViewController {
    
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var gender: UISegmentedControl!
    @IBOutlet weak var mpin1: UITextField!
    @IBOutlet weak var mpin2: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var contactNumber: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    private var fbid : String?
    var isRegister = false
    var withFacebook = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        if isRegister == true {
            
            profileImage.image = nil //placeholder
            
            let dic = NSUserDefaults.standardUserDefaults().objectForKey("fbdetails") as? NSDictionary
            if dic != nil {
                firstName.text = dic!["first_name"] as? String
                lastName.text = dic!["last_name"] as? String
                email.text = dic!["email"] as? String
                fbid = dic!["id"] as? String
                
                firstName.enabled = false
                lastName.enabled = false
                email.enabled = false
                contactNumber.becomeFirstResponder()
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if fbid != nil {
            appDelegate.showWait()
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                
                let img = UIImage(data: NSData(contentsOfURL: NSURL(string: "https://graph.facebook.com/\(self.fbid!)/picture?type=large")!)!)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.appDelegate.hideWait()
                    self.profileImage.image = img
                })
            })
            
            
        } else {
            self.tableView.tableHeaderView = nil
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func bdayEditAction(sender: AnyObject) {
        
    }
    
    @IBAction func addressEditAction(sender: AnyObject) {
        
    }

    @IBAction func saveAction(sender: AnyObject) {
        if validateFields() == true {
            if isRegister == true {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.appDelegate.hideWait()
                })
                
                APIManager.registerUser(self.appDelegate.urlSession!, params: ["name" : "\(firstName.text!) \(lastName.text!)", "email" : email.text!, "mobilenumber" : contactNumber.text!, "pin" : mpin1.text!, "fbid" : fbid == nil ? "" : fbid!, "merchantid" : APIManager.MerchantID, "profileimage" : fbid == nil ? "" : "https://graph.facebook.com/\(self.fbid!)/picture?type=large"], completion: { (data, error) -> Void in
                    
                    do {
                        let p = NSString(data: data, encoding: NSUTF8StringEncoding)
                        print(p)
                        let s = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                        print(s)
                        
                        let code = s["response"]!!["code"] as! Int
                        if code == 200 {
                            NSUserDefaults.standardUserDefaults().setObject(true, forKey: "loggedin")
                            
                            let name = s["response"]!!["data"]!![0]!["name"]
                            NSUserDefaults.standardUserDefaults().setObject(0, forKey: "points")
                            
                            NSUserDefaults.standardUserDefaults().setObject(name, forKey: "fullname")
                            NSUserDefaults.standardUserDefaults().synchronize()
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                                    
                                })
                            })
                        }

                        
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    } catch {}
                    
                    }, failure: { (data, error) -> Void in
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.appDelegate.hideWait()
                        })
                })
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if isRegister {
            saveButton.setTitle("REGISTER", forState: .Normal)
        }
    }
    
    func validateFields() -> Bool {
        var ret = true
        
        if firstName.text == nil || firstName.text!.characters.count == 0 {
            ret = false
        }
        
        if lastName.text == nil || lastName.text!.characters.count == 0 {
            ret = false
        }
        
        if contactNumber.text == nil || contactNumber.text!.characters.count != 11 {
            ret = false
        }
        
        if email.text == nil || email.text!.characters.count == 0 {
            ret = false
        }
        
        if email.text!.containsString("@") == false || email.text!.containsString(".") == false {
            ret = false
        }
        
        if mpin1.text == nil || mpin1.text?.characters.count == 0 {
            ret = false
        }
        
        if mpin2.text == nil || mpin2.text?.characters.count == 0 {
            ret = false
        }
        
        if mpin1.text! != mpin2.text! {
            ret = false
        }
        
        return ret
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 1 {
            return 5
        }
        
        return 2
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
