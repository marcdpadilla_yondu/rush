//
//  WelcomeViewController.swift
//  Rush
//
//  Created by Marc Darren Padilla on 28/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

extension ZBarSymbolSet: SequenceType {
    public func generate() -> NSFastGenerator {
        return NSFastGenerator(self)
    }
}

class WelcomeViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var fbButton: FBSDKLoginButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let loggedIn = NSUserDefaults.standardUserDefaults().objectForKey("loggedin")?.boolValue
        if loggedIn == true {
            self.performSegueWithIdentifier("PushHome", sender: self)
        } else {
            let fbManager = FBSDKLoginManager()
            fbManager.logOut()
            NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "fbdetails")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            APIManager.getAppDetails(appDelegate.urlSession!, params: ["merchantid" : APIManager.MerchantID, "designtype" : "3"], completion: { (data, error) -> Void in
                
                do {
                    let d = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                    let code = d["response"]!!["code"] as! Int
                    print(d)
                    if code == 200 {
                        let dic = d["response"]!!["data"]!![0] as! NSDictionary
                        let bgUrl = "\(APIManager.ServerIP.substringToIndex(APIManager.ServerIP.endIndex.advancedBy(-1)))\(dic["background"]!)"
                        let logoUrl = "\(APIManager.ServerIP.substringToIndex(APIManager.ServerIP.endIndex.advancedBy(-1)))\(dic["logo"]!)"
                        self.downloadImages(bgUrl, imageView: self.bgImage)
                        self.downloadImages(logoUrl, imageView: self.logo)
                    }
                    
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                
                }, failure: { (data, error) -> Void in
                    
            })
        }
    }
    
    func downloadImages(url : String, imageView : UIImageView) {
        if url.characters.count == 0 {
            return
        }
        
        let request = NSURLRequest(URL: NSURL(string: url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 60.0)
        let downloadTask = appDelegate.urlSession?.downloadTaskWithRequest(request, completionHandler: { (location, response, error) -> Void in
            
            if error == nil {
                let img = UIImage(data: NSData(contentsOfURL: location!)!)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    UIView.animateWithDuration(0.200, animations: { () -> Void in
                        imageView.alpha = 0.0
                        }, completion: { (_) -> Void in
                            imageView.image = img
                            UIView.animateWithDuration(0.200, animations: { () -> Void in
                                imageView.alpha = 1.0
                                }, completion: { (_) -> Void in
                                    
                            })
                    })
                })
                
            }
        })
        
        downloadTask?.resume()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fbButton.layer.cornerRadius = UIManager.buttonRadius
        fbButton.readPermissions = ["public_profile", "email", "user_friends", "user_birthday"]
        loginButton.layer.borderWidth = 1.0
        loginButton.layer.borderColor = UIColor.whiteColor().CGColor
        loginButton.layer.cornerRadius = UIManager.buttonRadius
        registerButton.layer.borderWidth = 1.0
        registerButton.layer.borderColor = UIColor.whiteColor().CGColor
        registerButton.layer.cornerRadius = UIManager.buttonRadius
        
        
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        appDelegate.showWait()
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id,email,name, first_name, last_name"]).startWithCompletionHandler { (connection, data, error) -> Void in
            if error == nil {
                
                var params : NSDictionary? = nil
                do {
                    let jsonData : NSData = try NSJSONSerialization.dataWithJSONObject(data, options: NSJSONWritingOptions.PrettyPrinted)
                    
                    params = try NSJSONSerialization.JSONObjectWithData(jsonData, options: .AllowFragments) as? NSDictionary
                } catch let error as NSError? {
                    print(error?.localizedDescription)
                } catch {}
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    APIManager.login(self.appDelegate.urlSession!, params: ["email" : params!["email"]!, "merchantid" : APIManager.MerchantID], completion: { (data, error) -> Void in
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.appDelegate.hideWait()
                        })
                        
                        do {
                            let d = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                            print(d)
                            let code = d["response"]!!["code"] as! Int
                            if (code != 9) {
                                let name = d["response"]!!["data"]!![0]!["name"]
                                let userid = d["response"]!!["data"]!![0]!["id"]
                                let points = d["response"]!!["data"]!![0]!["package"]!![0]!["value"] as? String
                                let qrcode = d["response"]!!["data"]!![0]!["qrImage"] as? String
                                if points == nil {
                                    NSUserDefaults.standardUserDefaults().setObject(0, forKey: "points")
                                } else {
                                    NSUserDefaults.standardUserDefaults().setObject(points, forKey: "points")
                                }
                                
                                NSUserDefaults.standardUserDefaults().setObject(name, forKey: "fullname")
                                NSUserDefaults.standardUserDefaults().setObject(userid, forKey: "id")
                                NSUserDefaults.standardUserDefaults().setObject(qrcode, forKey: "qrcode")
                                NSUserDefaults.standardUserDefaults().synchronize()
                                
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.appDelegate.hideWait()
                                    NSUserDefaults.standardUserDefaults().setObject(true, forKey: "loggedin")
                                    NSUserDefaults.standardUserDefaults().synchronize()
                                    
                                    self.performSegueWithIdentifier("PushHome", sender: self)
                                })
                            } else if code == 9 {
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.appDelegate.hideWait()
                                    NSUserDefaults.standardUserDefaults().setObject(false, forKey: "loggedin")
                                    if params != nil {
                                        NSUserDefaults.standardUserDefaults().setObject(params!, forKey: "fbdetails")
                                    }
                                    NSUserDefaults.standardUserDefaults().synchronize()
                                    
                                    if params != nil {
                                        self.registerAction(self.registerButton)
                                    }
                                    
                                })
                            }
                        } catch let error as NSError {
                            print(error.localizedDescription)
                        } catch {}
                        
                        }, failure: { (data, error) -> Void in
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.appDelegate.hideWait()
                                let fbManager = FBSDKLoginManager()
                                fbManager.logOut()
                            })
                    })
                    
                })
            }
            
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func registerUser(params : NSDictionary?) {
        APIManager.registerUser(self.appDelegate.urlSession!, params: ["name" : params!["name"]!, "email" : params!["email"]!, "mobileNumber" : "", "pin" : "", "pinConfirm" : ""], completion: { (data, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.appDelegate.hideWait()
            })
            
            do {
                let d = NSString(data: data, encoding: NSUTF8StringEncoding)
                print(d)
                
                let s = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                print(s)
            } catch let error as NSError {
                print(error.localizedDescription)
            } catch {}
            }, failure: { (data, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.appDelegate.hideWait()
                })
        })
    }

    @IBAction func loginAction(sender: AnyObject) {
        self.performSegueWithIdentifier("PushLogin", sender: nil)
        
    }
    
    @IBAction func registerAction(sender: AnyObject) {
        let a : EditProfileTableViewController = self.storyboard!.instantiateViewControllerWithIdentifier("registerProfile") as! EditProfileTableViewController
        let nav = UINavigationController(rootViewController: a)
        a.title = "REGISTER"
        a.isRegister = true
        UIManager.configureNavigationBar(a)
        
        let close = UIButton(type: .Custom)
        close.setImage(UIImage(named: "edit-backBtn"), forState: UIControlState.Normal)
        close.frame = CGRectMake(0, 0, 30.0, 30.0)
        close.addTarget(self, action: Selector("registerClose"), forControlEvents: .TouchUpInside)
        
        let barItem = UIBarButtonItem(customView: close)
        a.navigationItem.leftBarButtonItem = barItem
        
        self.presentViewController(nav, animated: true) { () -> Void in
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PushScan" {
            
        }
    }
    
    
    func registerClose() {
        self.presentedViewController!.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
