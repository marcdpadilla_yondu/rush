//
//  LoginViewController.swift
//  Rush
//
//  Created by Marc Darren Padilla on 15/1/16.
//  Copyright © 2016 Section 8. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var mobileField: UITextField!
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        UIManager.configureNavigationBar(self)
        
        let close = UIButton(type: .Custom)
        close.setImage(UIImage(named: "edit-backBtn"), forState: UIControlState.Normal)
        close.frame = CGRectMake(0, 0, 30.0, 30.0)
        close.addTarget(self, action: Selector("loginClose"), forControlEvents: .TouchUpInside)
        
        let barItem = UIBarButtonItem(customView: close)
        self.navigationItem.leftBarButtonItem = barItem
        
        self.title = "LOGIN"
    }
    
    func loginClose() {
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    @IBAction func loginAction(sender: AnyObject) {
        appDelegate.showWait()
        APIManager.login(self.appDelegate.urlSession!, params: ["mobilenumber" : mobileField.text!, "merchantid" : APIManager.MerchantID], completion: { (data, error) -> Void in
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.appDelegate.hideWait()
            })
            
            do {
                let d = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                print(d)
                let code = d["response"]!!["code"] as! Int
                if (code != 9) {
                    let name = d["response"]!!["data"]!![0]!["name"]
                    let userid = d["response"]!!["data"]!![0]!["id"]
                    let points = d["response"]!!["data"]!![0]!["package"]!![0]!["value"] as? String
                    let qrcode = d["response"]!!["data"]!![0]!["qrImage"] as? String
                    if points == nil {
                        NSUserDefaults.standardUserDefaults().setObject(0, forKey: "points")
                    } else {
                        NSUserDefaults.standardUserDefaults().setObject(points, forKey: "points")
                    }
                    
                    NSUserDefaults.standardUserDefaults().setObject(name, forKey: "fullname")
                    NSUserDefaults.standardUserDefaults().setObject(userid, forKey: "id")
                    NSUserDefaults.standardUserDefaults().setObject(qrcode, forKey: "qrcode")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.appDelegate.hideWait()
                        NSUserDefaults.standardUserDefaults().setObject(true, forKey: "loggedin")
                        NSUserDefaults.standardUserDefaults().synchronize()
                        
                        self.dismissViewControllerAnimated(true, completion: { () -> Void in
                            
                        })
                    })
                } else if code == 9 {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.appDelegate.hideWait()
                        let alert = UIAlertController(title: "Error", message: "Invalid login", preferredStyle: .Alert)
                        let ok = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                            
                        })
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: { () -> Void in
                            
                        })
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            } catch {}
            
            }, failure: { (data, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.appDelegate.hideWait()
                    
                    let alert = UIAlertController(title: "Error", message: "There was an error encountered when trying to login. Please try again later.", preferredStyle: .Alert)
                    let ok = UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                        
                    })
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: { () -> Void in
                        
                    })
                })
            })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
