//
//  ImageDownloader.swift
//  Rush
//
//  Created by Marc Darren Padilla on 3/2/16.
//  Copyright © 2016 Section 8. All rights reserved.
//

import UIKit

class ImageDownloader: NSObject {
    
    var objectModel : AnyObject?
    var completionHandler : ((Void) -> Void)?
    var urlSession : NSURLSession?
    
    var downloadTask : NSURLSessionDownloadTask?
    
    override init() {
        super.init()
    }
    
    func start() {
        
        if objectModel?.isKindOfClass(RedeemableProduct.self) == true {
            
            let om = objectModel as! RedeemableProduct
            
            let request = NSURLRequest(URL: NSURL(string: om.imageUrl!)!, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 60.0)
            
            downloadTask = urlSession?.downloadTaskWithRequest(request, completionHandler: { (url, response, error) -> Void in
                if error == nil {
                    let img = UIImage(data: NSData(contentsOfURL: url!)!)
                    
                    if img != nil {
                        om.image = img
                        if self.completionHandler != nil {
                            self.completionHandler!()
                        }
                    }
                } else {
                    print(error?.localizedDescription)
                    print(om.imageUrl!)
                }
            })
        }
        
        downloadTask?.resume()
    }
    
    func cancel() {
        self.downloadTask?.cancel()
        self.downloadTask = nil
    }
}
