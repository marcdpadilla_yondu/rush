//
//  RedeemTableViewController.swift
//  Rush
//
//  Created by Marc Darren Padilla on 29/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

class RedeemTableViewController: UITableViewController {
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    private var products : [RedeemableProduct]?
    private let pendingDownloads = NSMutableDictionary()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        products = [RedeemableProduct]()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        downloadProducts()
    }
    
    func downloadProducts() {
        appDelegate.showWait()
        let id = NSUserDefaults.standardUserDefaults().objectForKey("id") as? String
        if id == nil {
            return
        }
        
        APIManager.getRedeemableProducts(appDelegate.urlSession!, params: ["merchantid" : APIManager.MerchantID, "customerid" : "1"], completion: { (data, error) -> Void in
            self.appDelegate.hideWait()
            do {
                
                let dic = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                let code = dic["response"]!!["code"] as! Int
                if code == 200 {
                    self.products?.removeAll()
                    let items = dic["response"]!!["data"]
                    var redeemableItems = [RedeemableProduct]()
                    for item : NSDictionary in items as! [NSDictionary] {
                        let i = RedeemableProduct()
                        i.details = item["details"] as? String
                        i.productId = Int((item["id"] as? String)!)
                        let serverIp = APIManager.ServerIP
                        
                        i.imageUrl = "\(serverIp.substringToIndex(serverIp.endIndex.advancedBy(-1)))\((item["image"] as? String)!)"
                        i.requiredPoints = Int((item["requiredPoints"] as? String)!)
                        
                        redeemableItems.append(i)
                    }
                    self.products?.appendContentsOf(redeemableItems)
                }
                
            } catch let error as NSError? {
                
                print(error!.localizedDescription)
                
            } catch {}
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.reloadData()
            })
            
            }) { (data, error) -> Void in
                self.appDelegate.hideWait()
                print(error?.localizedDescription)
                
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (products?.count)!
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RedeemCell", forIndexPath: indexPath) as? RedeemTableViewCell

        return cell!
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let c  = cell as! RedeemTableViewCell
        configureCell(c, indexPath: indexPath)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200.0
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func configureCell(cell : RedeemTableViewCell, indexPath : NSIndexPath) {
        cell.pointsBg.layer.cornerRadius = 45.0
        
        let item = products![indexPath.row] as RedeemableProduct
        
        var pointsString = NSString(string: "\(item.requiredPoints!) \(item.requiredPoints! > 1 ? "POINTS" : "POINT")")
        pointsString = pointsString.stringByReplacingOccurrencesOfString(" ", withString: "\n")
        let attributedString = NSMutableAttributedString(string: pointsString as String, attributes: [NSFontAttributeName : cell.pointsLabel.font, NSForegroundColorAttributeName : cell.pointsLabel.textColor])
        attributedString.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(15), range: pointsString.rangeOfString(item.requiredPoints! > 1 ? "POINTS" : "POINT"))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.whiteColor(), range: pointsString.rangeOfString(item.requiredPoints! > 1 ? "POINTS" : "POINT"))
        cell.pointsLabel.attributedText = attributedString
        cell.promoLabel.text = item.details!
        
        if item.image == nil {
            if self.tableView.decelerating == false && self.tableView.dragging == false {
                downloadImage(item, indexPath: indexPath)
                
            }
            
            cell.bgImage.image = nil
            cell.backgroundColor = UIColor.grayColor()
        } else {
            cell.backgroundColor = UIColor.clearColor()
            cell.bgImage.image = item.image!
        }
        
        if cell.containsGradient == false {
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.frame = cell.descContainer.bounds
            gradient.colors = [UIColor.clearColor().CGColor, UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).CGColor]
            cell.descContainer.layer.insertSublayer(gradient, atIndex: 0)
            cell.containsGradient = true
        }
    }
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            self.loadImageForOnScreenRows()
        }
    }
    
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        self.loadImageForOnScreenRows()
    }
    
    func cancelAllDownloads() {
        let allDownloads = pendingDownloads.allValues as NSArray
        allDownloads.forEach { download in
            (download as! ImageDownloader).cancel()
        }
    }
    
    func downloadImage(item : RedeemableProduct, indexPath : NSIndexPath) {
        var download = pendingDownloads[indexPath] as? ImageDownloader
        if download == nil {
            download = ImageDownloader()
            download?.objectModel = item
            download?.urlSession = appDelegate.urlSession
            download?.completionHandler = { (Void) -> Void in
                let cell = self.tableView.cellForRowAtIndexPath(indexPath) as! RedeemTableViewCell
                cell.bgImage.image = item.image!
                self.pendingDownloads.removeObjectForKey(indexPath)
            }
        }
        self.pendingDownloads[indexPath] = download
        download?.start()
    }
    
    func loadImageForOnScreenRows() {
        if products?.count > 0 {
            let rows = self.tableView.indexPathsForVisibleRows! as [NSIndexPath]
            
            for indexPath : NSIndexPath in rows as [NSIndexPath] {
                let i = products![indexPath.row] as RedeemableProduct
                
                if i.image == nil {
                    downloadImage(i, indexPath: indexPath)
                }
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
