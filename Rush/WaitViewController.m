//
//  WaitViewController.m
//  Globe Ride
//
//  Created by Marc Darren Padilla on 23/5/15.
//  Copyright (c) 2015 Section 8. All rights reserved.
//

#import "WaitViewController.h"

@interface WaitViewController ()

@end

@implementation WaitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        window.windowLevel = UIWindowLevelAlert;
        window.rootViewController = self;
    }
    
    return self;
}

- (id) init {
    self = [super initWithNibName:@"WaitViewController" bundle:nil];
    if (self) {
        window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        window.windowLevel = UIWindowLevelAlert;
        window.rootViewController = self;
    }
    
    return self;
}

- (void) show {
    [window makeKeyAndVisible];
    
    [UIView animateWithDuration:.250 animations:^{
        self.progressContainer.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
}

- (void) hide {
    [UIView animateWithDuration:.250 animations:^{
        
        self.progressContainer.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        [window setHidden:YES];
        [[[UIApplication sharedApplication] delegate].window makeKeyAndVisible];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.progressContainer.alpha = 0.0;
    self.progressContainer.layer.cornerRadius = 10.0;
    bg = [[UIToolbar alloc] initWithFrame:self.progressContainer.bounds];
    bg.barStyle = UIBarStyleBlackTranslucent;
    self.progressContainer.backgroundColor = [UIColor clearColor];
    [self.progressContainer.layer insertSublayer:bg.layer atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
