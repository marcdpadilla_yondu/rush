//
//  HomeViewController.swift
//  Rush
//
//  Created by Marc Darren Padilla on 28/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, ZBarReaderDelegate {
    
    private var codeReader : ZBarReaderViewController?
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var notificationIcon: UIBarButtonItem!
    @IBOutlet weak var redeemButton: UIButton!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var promoImage: UIImageView!
    
    
    override func prefersStatusBarHidden() -> Bool {
        return false
    }
    
    override func preferredStatusBarUpdateAnimation() -> UIStatusBarAnimation {
        return .Slide
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    func customSetup() {
        UIManager.configureNavigationBar(self)
        pointsLabel.text = "\(NSUserDefaults.standardUserDefaults().objectForKey("points")!)PTS"
        let pointsString = NSString(string: pointsLabel.text!)
        let attributedString = NSMutableAttributedString(string: pointsString as String, attributes: [NSFontAttributeName : pointsLabel.font, NSForegroundColorAttributeName : pointsLabel.textColor])
        attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(20), range: pointsString.rangeOfString("PTS"))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.blackColor(), range: pointsString.rangeOfString("PTS"))
        attributedString.addAttribute(NSBaselineOffsetAttributeName, value: 17.0, range: pointsString.rangeOfString("PTS"))
        pointsLabel.attributedText = attributedString
        
        notificationIcon.badgeMinSize = 20.0
        notificationIcon.badgeFont = UIFont.systemFontOfSize(12.0)
        notificationIcon.badgePadding = 3.0
        redeemButton.layer.cornerRadius = UIManager.buttonRadius
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        customSetup()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let d = NSDate()
        let f = NSDateFormatter()
        f.dateFormat = "MMM. d yyyy"
        f.locale = NSLocale(localeIdentifier: "en_US")
        dateLabel.text = "As of \(f.stringFromDate(d))"
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        appDelegate.showWait()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
            let qrcode = NSUserDefaults.standardUserDefaults().objectForKey("qrcode") as? String
            if qrcode != nil {
                print("\(APIManager.ServerIP)\(qrcode!)")
                let img = UIImage(data: NSData(contentsOfURL: NSURL(string: "\(APIManager.ServerIP)\(qrcode!)")!)!)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.appDelegate.hideWait()
                    self.qrImage.image = img
                })
            } else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.appDelegate.hideWait()
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PushScan" {
            codeReader = segue.destinationViewController as? ZBarReaderViewController
            codeReader!.readerDelegate = self
            
            let scanner : ZBarImageScanner = codeReader!.scanner as ZBarImageScanner
            scanner.setSymbology(ZBAR_I25, config: ZBAR_CFG_ENABLE, to: 0)
            
            codeReader!.showsCameraControls = false;  // for UIImagePickerController
            codeReader!.showsZBarControls = false;
            
            let container = UIView(frame: self.view.bounds)
            container.backgroundColor = UIColor.clearColor()
            let overlay = UIView(frame: container.bounds)
            overlay.backgroundColor = UIColor.clearColor()
            
            let screenRect = UIScreen.mainScreen().bounds
            let path = UIBezierPath(rect: screenRect)
            let holePath = UIBezierPath(rect: CGRectMake((screenRect.size.width/2)-((screenRect.size.width-80.0)/2.0), (screenRect.size.height/2)-((screenRect.size.width-80.0)/2.0)-44.0, screenRect.size.width-80.0, screenRect.size.width-80.0))
            path.appendPath(holePath)
            path.usesEvenOddFillRule = true
            
            let fillLayer = CAShapeLayer()
            fillLayer.path = path.CGPath
            fillLayer.fillRule = kCAFillRuleEvenOdd
            fillLayer.fillColor = UIManager.navigationBarColor.CGColor
            fillLayer.opacity = 0.7
            overlay.layer.addSublayer(fillLayer)
            
            
            container.addSubview(overlay)
            
            codeReader!.cameraOverlayView = container;
            
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        picker.dismissViewControllerAnimated(true) { () -> Void in
            
        }
        /*
//  get the decode results
id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];

ZBarSymbol *symbol = nil;
for(symbol in results)
// just grab the first barcode
break;

symbol.data

*/
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        picker.dismissViewControllerAnimated(true) { () -> Void in

            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.appDelegate.showWait()
            })
            
            let results = info[ZBarReaderControllerResults] as! NSFastEnumeration

            var symbol : ZBarSymbol = ZBarSymbol()
            for s : ZBarSymbol in results as! [ZBarSymbol] {
                symbol = s
                break
            }

            symbol.data
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                APIManager.validateQRCode(self.appDelegate.urlSession!, params: ["qrCode" : symbol.data], completion: { (data, error) -> Void in
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.appDelegate.hideWait()
                        })
                    }, failure: { (data, error) -> Void in
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.appDelegate.hideWait()
                        })
                })
            })
            
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
