//
//  RedeemableProduct.swift
//  Rush
//
//  Created by Marc Darren Padilla on 2/2/16.
//  Copyright © 2016 Section 8. All rights reserved.
//

import UIKit

class RedeemableProduct: NSObject {

    var details : String?
    var imageUrl : String?
    var image : UIImage?
    var requiredPoints : Int?
    var productId : Int?
}
