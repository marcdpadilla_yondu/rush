//
//  RedeemTableViewCell.swift
//  Rush
//
//  Created by Marc Darren Padilla on 29/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

class RedeemTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var pointsBg: UIView!
    @IBOutlet weak var descContainer: UIView!
    @IBOutlet weak var promoLabel: UILabel!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var giftIcon: UIImageView!
    
    var containsGradient : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}