//
//  ProfileTableViewCell.swift
//  Rush
//
//  Created by Marc Darren Padilla on 28/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profileImage.clipsToBounds = true
//        profileImage.layer.cornerRadius = profileImage.frame.size.width/2.0
        profileImage.layer.borderWidth = 2.0
        profileImage.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImage.layer.cornerRadius = 45.0
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
