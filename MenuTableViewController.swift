//
//  MenuTableViewController.swift
//  Rush
//
//  Created by Marc Darren Padilla on 28/12/15.
//  Copyright © 2015 Section 8. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    
    private var menuItems : [String]?
    
    override func prefersStatusBarHidden() -> Bool {
        return false
    }
    
    override func preferredStatusBarUpdateAnimation() -> UIStatusBarAnimation {
        return .Slide
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .Default
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        menuItems = ["profile_summary", "Home", "Friends", "Transaction History", "Notifications", "FAQs", "Settings", "Logout"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 120.0
        }
        
        return 60.0
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (menuItems?.count)!
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell? = nil
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("ProfileCell", forIndexPath: indexPath) as! ProfileTableViewCell
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("MenuItemCell", forIndexPath: indexPath) as! MenuItemTableViewCell
        }

        configureCell(cell!, indexPath: indexPath)

        return cell!
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    override func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.row == 7 { // logout
            NSUserDefaults.standardUserDefaults().setObject(false, forKey: "loggedin")
            NSUserDefaults.standardUserDefaults().synchronize()
            self.revealViewController().navigationController?.popViewControllerAnimated(true)
        } else if indexPath.row == 1 { // home
            self.performSegueWithIdentifier("PushHome", sender: nil)
        } else if indexPath.row == 6 { // settings
            self.performSegueWithIdentifier("PushSettings", sender: nil)
        } else if indexPath.row == 2 { // friends
            self.performSegueWithIdentifier("PushFriends", sender: nil)
        }
    }
    
    func configureCell(cell : UITableViewCell?, indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            let c : ProfileTableViewCell = cell as! ProfileTableViewCell
//            c.profileImage.layer.cornerRadius = 45.0
//            c.profileImage.layer.cornerRadius = c.profileImage.frame.size.width/2.0
            
            c.nameLabel.text = NSUserDefaults.standardUserDefaults().objectForKey("fullname") as? String
            c.pointsLabel.text = "\(NSUserDefaults.standardUserDefaults().objectForKey("points")!)Pts"
            
        } else {
            let c : MenuItemTableViewCell = cell as! MenuItemTableViewCell
            c.menuTitleLabel.text = menuItems![indexPath.row]
            
            switch indexPath.row {
            case 1:
                c.menuIcon.image = UIImage(named: "home")
                break;
            case 2:
                c.menuIcon.image = UIImage(named: "friends")
                break;
            case 3:
                c.menuIcon.image = UIImage(named: "transactionHistory")
                break;
            case 4:
                c.menuIcon.image = UIImage(named: "notifications")
                break;
            case 5:
                c.menuIcon.image = UIImage(named: "FAQ")
                break;
            case 6:
                c.menuIcon.image = UIImage(named: "settingsSideBar")
                break;
            case 7:
                c.menuIcon.image = UIImage(named: "logOut")
                break;
            default:
                break;
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
